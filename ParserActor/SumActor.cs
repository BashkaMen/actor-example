﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParserActor
{
    public class SumActor : AbstractActor<int>
    {
        protected override int ThreadCount => 1;


        private int State { get; set; }

        protected override Task HandleItem(int message)
        {
            State += message;

            return Task.CompletedTask;
        }

        public int GetState() => State;
    }
}
